<html>
    <head>
        <title>Baltic talents- @yield('title')</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonym ous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
            crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    </head>
    <body>
        <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Baltic talents</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <form action="{{ route('language_change')}}" method="post">
                        <input type="hidden" name="language" value="lt" />
                        <input type="submit" value="LT" />
                        {{csrf_field()}}
                    </form>
                </li>
                <li>
                    <form action="{{ route('language_change')}}" method="post">
                        <input type="hidden" name="language" value="en" />
                        <input type="submit" value="EN" />
                        {{csrf_field()}}
                    </form>
                </li>
                <li><a href="#">{{trans('messages.You_are_logged_in', ['name' => Auth::user()->name])}}</a></li>
            </ul>
        </div>
        </nav>
        <div class="container">
        @if( Session::has( 'success' ))
            <div class="alert alert-success">
                {{ Session::get( 'success' ) }}
            </div>
        @elseif( Session::has( 'errors' ))
            <div class="alert alert-danger">
            @foreach (Session::get('errors')->all() as $error)
            {{$error}}
            @endforeach
            </div>
        @endif
            @yield('content')
        </div>
    </body>
</html>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    public function form(Request $request)
    {
        return view('document_form');
    }

    public function list(Request $request)
    {
        return view('document_list', ['documents' => Document::all()]);
    }

    public function upload(Request $request)
    {
        $file = $request->file('document');
        if (!$file) {
            return \Redirect::back()->withErrors(["Nepavyko išsaugoti failo"]);
        }
        
        $original_name = $file->getClientOriginalName();
        
        Storage::put('', $file);

        $document = new Document();
        $document->original_name = $original_name;
        $document->path = $file->hashName();
        $document->save();

        return \Redirect::back()->withSuccess( "Failas išsaugotas" );
    }

    public function download(Document $document, Request $request)
    {
        return response()->download(
            Storage::path($document->path),
            $document->original_name
        );
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']], function () {
    Route::get('/owner/list', 'OwnerController@list');
    Route::get('comment/{id}', 'CommentController@view');
});

Route::get('car/{car}/edit', 'CarController@edit')->name('car_edit')->middleware('can:update,car');
Route::post('car/{car}/update', 'CarController@update')->name('car_update')->middleware('can:update,car');

Route::post('language/change', 'LanguageController@change')->name('language_change');

Route::resource('education', 'EducationController');

Route::get('document/form', 'DocumentController@form')->name('document_form')->middleware('can:upload,App\Document');
Route::get('document/list', 'DocumentController@list')->name('document_list')->middleware('can:list,App\Document');
Route::get('document/{document}/download', 'DocumentController@download')->name('document_download')->middleware('can:download,document');
Route::post('document/upload', 'DocumentController@upload')->name('document_upload')->middleware('can:upload,App\Document');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
